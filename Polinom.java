package Polinom;

import java.util.ArrayList;
import java.util.Iterator;

public class Polinom {

    protected ArrayList<Monom> polinomArray = new ArrayList<Monom>();

    public Polinom(ArrayList<Monom> polinomArray){
        this.polinomArray=polinomArray;
    }

    public Polinom() {

    }

    public void setPolinomArray(ArrayList<Monom> polinomArray) {
        this.polinomArray = polinomArray;
    }

    public ArrayList<Monom> getPolinomArray() {
        return polinomArray;
    }

    public ArrayList<Monom> adunaTermeniiAsemenea(ArrayList<Monom> rezultat){

        for(int i=0;i<rezultat.size();i++){
            for(int j=i+1;j<rezultat.size();j++) {       //caut monoame de aceeasi putere
                Monom m1=rezultat.get(i);
                Monom m2=rezultat.get(j);
                if(m1.putere==m2.putere){
                    m1.setCoef(m1.coef+m2.coef);     //adun coeficientii pentru termenii cu aceeasi putere si atribui primului monom
                    rezultat.remove(m2);        //sterg al doilea monom  de aceeasi putere
                    j--;
                }
            }
        }
        return rezultat;
    }

    public ArrayList<Monom> adunaPolinoame(Polinom p2){
        ArrayList<Monom> rezultat= new ArrayList<Monom>();

        for (Monom m: this.polinomArray) {      //copiez in arrayListul rezzultat primul polinom (p1)
            Monom monom = new Monom(m.getCoef(),m.getPutere());
            rezultat.add(monom);
        }

        for (Monom m: p2.polinomArray) {        //copiez in arrayListul rezultat al doilea polinom (p2)
            Monom monom = new Monom(m.getCoef(),m.getPutere());
            rezultat.add(monom);
        }

        return adunaTermeniiAsemenea(rezultat);   //adun termenii asemenea
    }

    public ArrayList<Monom> scadePolinoame(Polinom p2){
        ArrayList<Monom> rezultat= new ArrayList<Monom>();

        for (Monom m: this.polinomArray) {    //copiez in arrayListul rezzultat primul polinom (p1)
            Monom monom = new Monom(m.getCoef(),m.getPutere());
            rezultat.add(monom);
        }

        rezultat=adunaTermeniiAsemenea(rezultat);    //pe primul polinom (p1) aplic metoda de adunare a termenilor cu aceeasi putere

        for (Monom m: p2.polinomArray) {       //copiez in arrayListul rezultat al doilea polinom (p2) , dar cu semn schimbat
            Monom monom = new Monom(-m.getCoef(),m.getPutere());
            rezultat.add(monom);
        }

        return adunaTermeniiAsemenea(rezultat);  //adun termenii asemenea
    }

    public ArrayList<Monom> inmultestePolinoame(Polinom p2){
        ArrayList<Monom> rezultat = new ArrayList<Monom>();
        for(Monom m1: this.polinomArray){        //inmultesc p1 cu p2 (monom cu monom)
            for(Monom m2: p2.polinomArray){
                Monom m = new Monom(m1.coef*m2.coef,m1.putere+m2.putere);
                rezultat.add(m);
            }
        }

        return adunaTermeniiAsemenea(rezultat);  //adun termenii asemenea

    }

    public ArrayList<Monom> deriveazaPolinom(){
        ArrayList<Monom> rezultat = new ArrayList<Monom>();
        for(Monom m: this.polinomArray){
            Monom monom = new Monom(m.coef*m.putere,--m.putere);  //derivez fiecare monom si il adaug in lista
            rezultat.add(monom);
        }

        return adunaTermeniiAsemenea(rezultat);

    }

    public ArrayList<Monom> integreazaPolinom(){
        ArrayList<Monom> rezultat = new ArrayList<Monom>();
        for(Monom m: this.polinomArray){
            int nouaPutere=m.putere+1;
            Monom monom = new Monom(m.coef/nouaPutere,nouaPutere);
            rezultat.add(monom);
        }

        return adunaTermeniiAsemenea(rezultat);

    }

    public String toString() {
        String polinomString="";
        for(Monom m: polinomArray) {
            if (m.coef < 0){
                if(m.putere==1) {
                    polinomString = polinomString + m.coef + "x";    //sub forma -5x
                } else {
                    if (m.putere == 0) {
                        polinomString = polinomString + m.coef;      //sub forma -5
                    }else {
                        polinomString = polinomString + m.toString();     //sub forma -5x^2
                    }
                }
            } else if(m.coef==0) {
                    polinomString=polinomString+"";                               //daca in urma operatiilor coeficientul devine 0
                } else {
                    if (m.putere == 1) {
                        polinomString = polinomString + "+" + m.coef + "x";     //sub forma 5x
                    } else if (m.putere == 0) {
                        polinomString = polinomString + "+" + m.coef;      //sub forma 5
                    } else {
                        polinomString = polinomString + "+" + m.toString();    // //sub forma 5x^2
                    }
                }
        }
        return polinomString;
    }
}
