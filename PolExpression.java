package Polinom;

import Polinom.Monom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolExpression{
    public PolExpression(){

    }

    public ArrayList<Monom> proceseazaTextPolinom(String input){
        String polPatt = "([+-]?\\d*)x(\\^(\\d+))?|([+-]\\d+)";
        Pattern pat = Pattern.compile(polPatt);
        Matcher m = pat.matcher(input);

        ArrayList<Monom> polArray = new ArrayList<Monom>();
        Iterator<Monom> i= polArray.iterator();

        while(m.find()) {
            Monom monom=new Monom();
            setMonomProcesat(m,monom);
            polArray.add(monom);
        }
        return polArray;
    }

    public void setMonomProcesat(Matcher m,Monom monom){
        int p;
        float c;
        if(m.group(4)==null){
            if(m.group(3)== null){
                p=1;     //puterea 1
            } else {
                p=Integer.parseInt(m.group(3));     // putere >1
            }
            if(m.group(1).equals("+") || m.group(1).equals("")){
                c=1;        // coeficientul 1
            } else{
                if(m.group(1).equals("-")) {
                    c = -1;      //coeficientul -1
                } else{
                    c=Float.parseFloat(m.group(1));   //coeficient diferit de -1 sau 1
                }
            }
            monom.setCoef(c);      // monom cu putere p si coeficient c
            monom.setPutere(p);
        } else{
            monom.setCoef(Float.parseFloat(m.group(4)));        //daca nu are x (puterea 0)
            monom.setPutere(0);
        }
    }
}