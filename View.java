package GUI;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JPanel bigPanel = new JPanel();
    private JPanel operatii2 = new JPanel();   //panel pentru butoanele de derivare si integrare
    private JPanel stanga = new JPanel();       //panel pentru labels
    private JPanel dreapta = new JPanel();     // panel pentru operatii2
    private JPanel centru = new JPanel();      //panel pentru text fields si operatii 1
    private JPanel operatii = new JPanel();   // panel pentru operatiile + - * /
    private JTextField polText1 = new JTextField(5);
    private JTextField polText2 = new JTextField(5);
    private JTextField polText = new JTextField(5);
    private JLabel polinom1 = new JLabel("P1: ");
    private JLabel polinom2 = new JLabel("P2: ");
    private JLabel polinom = new JLabel("P:");
    private JLabel egal = new JLabel("=");
    private JButton adunare = new JButton("+");
    private JButton scadere = new JButton("-");
    private JButton inmultire = new JButton("*");
    private JButton impartire = new JButton("/");
    private JButton derivare = new JButton(" '");
    private JButton integrare= new JButton("∫");

    public View() {
        this.setTitle("Polinoame");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(450, 300);
        this.setLocation(580, 300);

        stanga.setLayout(new BoxLayout(stanga, BoxLayout.Y_AXIS));
        dreapta.setLayout(new BoxLayout(dreapta, BoxLayout.Y_AXIS));
        centru.setLayout(new BoxLayout(centru, BoxLayout.Y_AXIS));
        operatii.setLayout(new BoxLayout(operatii, BoxLayout.X_AXIS));
        operatii2.setLayout(new BoxLayout(operatii2, BoxLayout.X_AXIS));

        bigPanel.setLayout(new BorderLayout());

        adaugaComponente();

        bigPanel.add(centru, BorderLayout.CENTER);
        bigPanel.add(stanga, BorderLayout.WEST);
        bigPanel.add(dreapta, BorderLayout.EAST);
        add(bigPanel);

        adaugaCulori();

        this.setVisible(true);
    }

    public void adaugaOperatii(){   // adauga operatiile + - * / pe panelul central
        operatii.add(adunare);                                                  //operatii
        operatii.add(Box.createRigidArea(new Dimension(10, 0)));
        operatii.add(scadere);
        operatii.add(Box.createRigidArea(new Dimension(10, 0)));
        operatii.add(inmultire);
        operatii.add(Box.createRigidArea(new Dimension(10, 0)));
        operatii.add(impartire);
        centru.add(operatii);
    }

    public void adaugaOperatii2(){   // adauga operatiile de derivare si integrare pe panelul dreapta
        operatii2.add(Box.createRigidArea(new Dimension(10, 0)));  //operatii2
        operatii2.add(derivare);
        operatii2.add(Box.createRigidArea(new Dimension(10, 0)));
        operatii2.add(integrare);
        operatii2.add(Box.createRigidArea(new Dimension(10, 0)));
        dreapta.add(operatii2);
    }

    public void adaugaComponente() {
        stanga.add(Box.createRigidArea(new Dimension(100, 28)));     //stanga
        stanga.add(polinom1);
        stanga.add(Box.createRigidArea(new Dimension(100, 82)));
        stanga.add(polinom2);
        stanga.add(Box.createRigidArea(new Dimension(100, 55)));
        stanga.add(polinom);

        dreapta.add(Box.createRigidArea(new Dimension(80, 25)));    //dreapta
        adaugaOperatii2();

        centru.add(Box.createRigidArea(new Dimension(0, 20)));    //centru
        centru.add(polText1);
        centru.add(Box.createRigidArea(new Dimension(0, 20)));
        adaugaOperatii();
        centru.add(Box.createRigidArea(new Dimension(0, 20)));    //centru
        centru.add(polText2);
        centru.add(Box.createRigidArea(new Dimension(0, 10)));
        centru.add(egal);
        centru.add(Box.createRigidArea(new Dimension(0, 10)));
        centru.add(polText);
        centru.add(Box.createRigidArea(new Dimension(0, 40)));
    }

    public void adaugaCulori() {
        operatii.setBackground(Color.decode("#66CDAA"));
        stanga.setBackground(Color.decode("#66CDAA"));
        dreapta.setBackground(Color.decode("#66CDAA"));
        centru.setBackground(Color.decode("#66CDAA"));
        operatii2.setBackground(Color.decode("#66CDAA"));
        bigPanel.setBackground(Color.decode("#66CDAA"));
        polText.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
        polText1.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
        polText2.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
        adunare.setBackground(Color.decode("#008080"));
        scadere.setBackground(Color.decode("#008080"));
        inmultire.setBackground(Color.decode("#008080"));
        impartire.setBackground(Color.decode("#008080"));
        derivare.setBackground(Color.decode("#008080"));
        integrare.setBackground(Color.decode("#008080"));
        polText.setBackground(Color.decode("#E0FFFF"));
        polText1.setBackground(Color.decode("#E0FFFF"));
        polText2.setBackground(Color.decode("#E0FFFF"));

    }

    public String getPolText1() {
        return polText1.getText();
    }

    public String getPolText2(){
        return polText2.getText();
    }

    public String getPolText(){
        return polText.getText();
    }
    public void adunareButtonListener(ActionListener ev){
        adunare.addActionListener(ev);
    }
    public void scadereButtonListener(ActionListener ev){
        scadere.addActionListener(ev);
    }
    public void inmultireButtonListener(ActionListener ev){
        inmultire.addActionListener(ev);
    }
    public void derivareButtonListener(ActionListener ev){
        derivare.addActionListener(ev);
    }
    public void integrareButtonListener(ActionListener ev){
        integrare.addActionListener(ev);
    }

    public void setPolText(String s){
        this.polText.setText(s);
    }
}