package Polinom;

public class Monom implements Comparable<Monom>{
    protected float coef;
    protected int putere;

    public Monom (float coef, int putere)
    {
        this.coef=coef;
        this.putere=putere;
    }

    public Monom (){

    }

    public float getCoef() {
        return coef;
    }

    public void setCoef(float coef) {
        this.coef = coef;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    @Override
    public int compareTo(Monom m) {
        int compPutere=((Monom)m).getPutere();  // descrescator
        return compPutere-this.putere;
    }

    public String toString() {
        return coef+ "x^"+putere;
    }
}
