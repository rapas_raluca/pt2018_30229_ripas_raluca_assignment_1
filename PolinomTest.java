package Polinom;

import Polinom.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class PolinomTest {

    Polinom p1= new Polinom();
    Polinom p2= new Polinom();


    void verifica(Polinom p, Polinom pExpected){
        Collections.sort(p.polinomArray);

        Iterator<Monom> i1=p.polinomArray.iterator();
        Iterator<Monom> i2=pExpected.polinomArray.iterator();

        while (i1.hasNext() && i2.hasNext()) {
            Monom m = (Monom) i1.next();
            Monom m2 = (Monom) i2.next();
            assertEquals(m.getCoef(), m2.getCoef());
            assertEquals(m.getPutere(), m2.getPutere());
        }
    }


    @Test
    void adunaTermeniiAsemenea() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> p2List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(2,3));             //x^3+2x^3+3x-x+4+3
        p1List.add(new Monom(3,1));
        p1List.add(new Monom(-1,1));
        p1List.add(new Monom(4,0));
        p1List.add(new Monom(3,0));

        p1.setPolinomArray(p1List);
        Polinom p= new Polinom(p1.adunaTermeniiAsemenea(p1.polinomArray));

        pExpectedList.add(new Monom(3,3));
        pExpectedList.add(new Monom(2,1));          //3x^3+2x+7
        pExpectedList.add(new Monom(7,0));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);

    }


    @Test
    void adunaPolinoame() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> p2List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(2,2));             //x^3+2x^2+3x-x-6
        p1List.add(new Monom(3,1));
        p1List.add(new Monom(-1,1));
        p1List.add(new Monom(-6,0));

        p2List.add(new Monom(4,4));
        p2List.add(new Monom(2,3));             //4x^4+2x^3-2x^2+2x+10
        p2List.add(new Monom(-2,2));
        p2List.add(new Monom(10,0));
        p2List.add(new Monom(2,1));
        p1.setPolinomArray(p1List);
        p2.setPolinomArray(p2List);
        Polinom p= new Polinom(p1.adunaPolinoame(p2));

        pExpectedList.add(new Monom(4,4));
        pExpectedList.add(new Monom(3,3));          //4x^4+3x^3+0x^2+4x+4
        pExpectedList.add(new Monom(0,2));
        pExpectedList.add(new Monom(4,1));
        pExpectedList.add(new Monom(4,0));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);
    }

    @Test
    void scadePolinoame() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> p2List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(2,2));         //x^3+2x^2+3x-x-6
        p1List.add(new Monom(3,1));
        p1List.add(new Monom(-1,1));
        p1List.add(new Monom(-6,0));

        p2List.add(new Monom(4,4));
        p2List.add(new Monom(2,3));         //4x^4+2x^3-2x^2+2x+10
        p2List.add(new Monom(-2,2));
        p2List.add(new Monom(10,0));
        p2List.add(new Monom(2,1));
        p1.setPolinomArray(p1List);
        p2.setPolinomArray(p2List);
        Polinom p= new Polinom(p1.scadePolinoame(p2));

        pExpectedList.add(new Monom(-4,4));
        pExpectedList.add(new Monom(-1,3));
        pExpectedList.add(new Monom(4,2));         //-4x^4-x^3+4x^2+0x-16
        pExpectedList.add(new Monom(0,1));
        pExpectedList.add(new Monom(-16,0));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);
    }

    @Test
    void inmultestePolinoame() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> p2List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(1,1));         //x^3+x+2
        p1List.add(new Monom(2,0));

        p2List.add(new Monom(2,3));
        p2List.add(new Monom(-2,2));         //2x^3-2x^2+1
        p2List.add(new Monom(1,0));
        p1.setPolinomArray(p1List);
        p2.setPolinomArray(p2List);
        Polinom p= new Polinom(p1.inmultestePolinoame(p2));

        pExpectedList.add(new Monom(2,6));
        pExpectedList.add(new Monom(-2,5));
        pExpectedList.add(new Monom(2,4));         //2x^6-2x^5+2x^4+3x^3-4x^2+x+2
        pExpectedList.add(new Monom(3,3));
        pExpectedList.add(new Monom(-4,2));
        pExpectedList.add(new Monom(1,1));
        pExpectedList.add(new Monom(2,0));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);
    }

    @Test
    void deriveazaPolinom() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(-2,2));         //x^3-2x^2+3x-x-6
        p1List.add(new Monom(3,1));
        p1List.add(new Monom(-1,1));
        p1List.add(new Monom(-6,0));

        p1.setPolinomArray(p1List);
        Polinom p= new Polinom(p1.deriveazaPolinom());

        pExpectedList.add(new Monom(3,2));
        pExpectedList.add(new Monom(-4,1));         //3x^2-4x+2
        pExpectedList.add(new Monom(2,0));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);
    }

    @Test
    void integreazaPolinom() {
        ArrayList<Monom> p1List= new ArrayList<Monom>();
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        p1List.add(new Monom(1,3));
        p1List.add(new Monom(-2,2));         //x^3-2x^2+3x-6
        p1List.add(new Monom(3,1));
        p1List.add(new Monom(-6,0));

        p1.setPolinomArray(p1List);
        Polinom p= new Polinom(p1.integreazaPolinom());

        pExpectedList.add(new Monom((float)0.25,4));
        pExpectedList.add(new Monom((float)-0.6666667,3));         //0.25x^4-0.6666667x^3+1.5x^2-6x
        pExpectedList.add(new Monom((float)1.5,2));
        pExpectedList.add(new Monom(-6,1));

        Polinom pExpected= new Polinom(pExpectedList);
        verifica(p,pExpected);
    }


    @Test
    void proceseazaTextPolinomTest(){
        String input = "4x^4+2x^2+3x+4";
        ArrayList<Monom> pExpectedList=new ArrayList<Monom>();
        PolExpression polinomExpr=new PolExpression();
        Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(input));

        pExpectedList.add(new Monom(4,4));
        pExpectedList.add(new Monom(2,2));
        pExpectedList.add(new Monom(3,1));
        pExpectedList.add(new Monom(4,0));

        Polinom pExpected= new Polinom(pExpectedList);
        Collections.sort(p1.polinomArray);
        Collections.sort(pExpected.polinomArray);
        verifica(p1,pExpected);
    }
}