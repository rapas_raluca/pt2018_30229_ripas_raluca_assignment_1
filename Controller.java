package GUI;

import GUI.View;
import Polinom.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

public class Controller {
    private View theView;
    private Polinom p1=new Polinom();
    private Polinom p=new Polinom();
    private Polinom p2=new Polinom();
    private PolExpression polinomExpr=new PolExpression();

    public Controller(Polinom p1, Polinom p, Polinom p2, View theView){
        this.theView=theView;
        this.p=p;
        this.p1=p1;
        this.p2=p2;

        theView.adunareButtonListener(new adunareButtonListener());
        theView.scadereButtonListener(new scadereButtonListener());
        theView.inmultireButtonListener(new inmultireButtonListener());
        theView.derivareButtonListener(new derivareButtonListener());
        theView.integrareButtonListener(new integrareButtonListener());

    }

    private class adunareButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                String pol1=theView.getPolText1();
                String pol2=theView.getPolText2();
                Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(pol1));
                Polinom p2=new Polinom( polinomExpr.proceseazaTextPolinom(pol2));
                Polinom p=new Polinom(p1.adunaPolinoame(p2));

                Collections.sort(p.getPolinomArray());
                theView.setPolText(p.toString());

            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class scadereButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                String pol1=theView.getPolText1();
                String pol2=theView.getPolText2();
                Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(pol1));
                Polinom p2=new Polinom( polinomExpr.proceseazaTextPolinom(pol2));
                Polinom p=new Polinom(p1.scadePolinoame(p2));

                Collections.sort(p.getPolinomArray());
                theView.setPolText(p.toString());

            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class inmultireButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                String pol1=theView.getPolText1();
                String pol2=theView.getPolText2();
                Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(pol1));
                Polinom p2=new Polinom( polinomExpr.proceseazaTextPolinom(pol2));
                Polinom p=new Polinom(p1.inmultestePolinoame(p2));

                Collections.sort(p.getPolinomArray());
                theView.setPolText(p.toString());

            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class derivareButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {

            String pol1=theView.getPolText1();   //pun in pol1 ce se afla in textfield
            Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(pol1));   // despart textul in  monoame
            Polinom p=new Polinom(p1.deriveazaPolinom());

            Collections.sort(p.getPolinomArray());
            theView.setPolText(p.toString());
        }
    }

    private class integrareButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {

            String pol1=theView.getPolText1();   //pun in pol1 ce se afla in textfield
            Polinom p1=new Polinom( polinomExpr.proceseazaTextPolinom(pol1));   // despart textul in  monoame
            Polinom p=new Polinom(p1.integreazaPolinom());

            Collections.sort(p.getPolinomArray());
            theView.setPolText(p.toString());
        }
    }

}
